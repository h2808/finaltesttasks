#include <iostream>
#include"UI.h"
using namespace std;

void Interface() 
{
    cout << "Program adds an element to each element of the current row \nof a two - dimensional array, belonging to the same row and side diagonal." << endl;

    const int N = 4;
    int A[N][N] = { {5,4,3,2},{9,8,7,4},{9,5,1,4},{9,2,5,7} };
    cout << "\nOriginal array looks this way: " << endl;
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            cout << A[i][j] << "\t ";
        }
        cout << endl;
    }
    cout << endl;
    cout << "Modified array looks this way: " << endl;
    for (int i = 0; i < N; i++) {
        int z = A[i][N - i - 1];
        for (int j = 0; j < N; j++) {
            int result = A[i][j];
            result += z;
            cout << result << "\t ";
        }
        cout << endl;
    }
 }