﻿#include <iostream>
using namespace std;
/*
int main()
{
//Найти сумму всех чисел кратных трем диапазона [1,50].
    cout << "The program calculates the sum of all multiples of three in the range [1,50]."<<endl;
    int SUM = 0;
    for (int K = 1; K < 50; K++) {
        if (K % 3 == 0)
            SUM += K;
    }
    cout << "The sum is: " << SUM;
}



int main()
{
//Вычислить сумму по формуле.

    double y = 0, sum = 0;
    int k = 1, n = 0;
    cout << "Enter border value: ";
    cin >> n;
    while (k <= n) {
        y = (pow((-1), k + 1)) / (k * (k + 1));
        sum += y;
        k++;
    }
    cout << "The sum is: " << sum;

}
*/
int main()
{
//Вычислить значение функции на заданном интервале.
    cout << "The program calculates the value of the function on the interval [-4,4] with 0.2 step." << endl;
    float x = -4, y = 0;
    do {
        y = sin(x) / (2 + x);
        x += 0.2;
        cout << "y= " << y << endl;
    } while (x <= 4);

}

