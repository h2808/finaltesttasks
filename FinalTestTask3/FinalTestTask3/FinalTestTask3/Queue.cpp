#include <iostream>
#include "struct.h"

void create(TNode** pBegin, TNode** pEnd, int d) {
    *pBegin = new TNode;
    (*pBegin)->d = d;
    (*pBegin)->pNext = NULL;
    *pEnd = *pBegin;
}

void add(TNode** pEnd, int d) {
    TNode* pv = new TNode;
    pv->d = d;
    pv->pNext = NULL;
    (*pEnd)->pNext = pv;
    *pEnd = pv;
}

int del(TNode** pBegin) {
    int tmp = (*pBegin)->d;
    TNode* pv = *pBegin;
    *pBegin = (*pBegin)->pNext;
    delete pv;
    return tmp;
}