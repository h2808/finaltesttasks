#include <iostream>
#include "Struct.h"
#include "Queue.h"

using namespace std;

void Interface()
{
    cout << "This program calculates the sum of elements that are multiples of two,\nfrom the set of numbers added to the queue." << endl;
    TNode* pBegin = NULL;
    TNode* pEnd = NULL;
    int x = 0, y = 0;
    float result = 0, sred = 0;
    cout << "Enter the number of items in the queue: ";
    cin >> y;
    if (y > 0)
    create(&pBegin, &pEnd, y);
    else {
        cout << "Data invalid... " << endl;
        return ;
    }
    for (int i = 0; i < y; i++) {
        add(&pEnd, y);
        char m[] = "the first", k[] = "next";
        if (i == 0)
            cout << "Enter" << " " << m << " " << "number : ";
        else
            cout << "Enter" << " " << k << " " << "number : ";
        cin >> x;
        if (x % 2 == 0)
            result += x;
    }
    if (result == 0)
        cout << "There are no elements that are multiples of two...";
    else cout << "The sum of the queue elements that are multiples of two is: " << result << ".";

    for (int i = 0; i < y; i++) {
        del(&pBegin);
    }
}
